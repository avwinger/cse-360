/**
* @author [Nikhil Netraganti,Jason Pajas,Andrew Wingersky,Logan Jepson, Nicholas Creasman ]
* @version [1.1]
* @param[DictionaryDemo][Initialises an GUI applet that contains Swing component and initialises and defines their functions]
* @return[void value When the code is run an GUI applet is created]
* @exception[IOException][IOException will be thrown if unable to populate a dictionary object given an incorrect file name] 
* @exception[FileNotFound][FileNotFound will be thrown when the input or dictionary file is not found]
 */



/*All the libraries used in the GUI*/
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;

/**This is the beginning of the DictionaryDemo class 
 * The class creates a GUI and uses Dictionary.java 
 * to allow users to compare two text files */
public class DictionaryDemo extends JPanel		
                      implements ListSelectionListener {
    
	/*Declaring all the variables and components used in the GUI*/
	private JList list;
    private DefaultListModel listModel;
    /*All the Strings used in the buttons of the gui are declared and initialized 
     * here for easy understanding*/
    private static final String addlist = "Add from list to Dictionary";
    private static final String addString = "Add New Word to List";
    private static final String helpString = "Help";
    private static final String dictString = "Enter Dictionary File Name";
    private static final String fileString = "Enter Input File Name";
    private static final String ignoreString = "Ignore";
    private static final String compareString = "Compare Chosen files";
    public static final String replaceString = "Replace words in Input File";
    public String DictfileName="empty";
    public String textfileName="empty";
    public String directionary="/Users/LoganJepson/Documents/";
    public String HelpText = "The Application compares the Input text file with the Dictionary\n"+
    						 "and takes all the words that are different and displays it in a list\n"+
    						 "and the user is able to then choose a particular word from the list and\n"+
    						 "Perform the following action on the selection:\n"+
    						 "Add to Dictionary: Add the selected word to the chosen dictionary file\n"+
    						 "Ignore: Ignore the word and remove it from the list\n"+
    						 "Add new word to List: Takes the word in the textfield and adds it to the list\n"+
    						 "Help: displays this window!\n\n"+
    						 "Furthermore the Dictionary file name is entered by pressing the 'Enter Dictionary File name button'\n"+
    						 "and the text file by pressing the 'Enter Text File name' button\n"+
    						 "The files are only compared when the 'Compare Files button is pressed.'\n"+
    						 "END.";
    						
    /*All the Java swing components used to make GUI*/
    private JButton addList;
    private JButton addto;
    private JButton help;
    private JButton dict;
    private JButton file;
    private JButton compare;
    private JButton stats;
    private JTextField Addwords;
    private JButton ignore;
    private JButton replace;
    public int wordsinFile=0;
	public int wordstoDictionary=0;
	public int wordsIgnored=0;
	public int linesreadInput=0;
    Dictionary dictionary = new Dictionary();
    public File dictionaryfile;
    public File textFile;
    
    ArrayList<String> ignoreArr = new ArrayList<String>();

    /**Constructor for the class
     * All the above declared components are initialized in the constructor*/
    public DictionaryDemo() {
        super(new BorderLayout());

        /*Declaring the List*/
        listModel = new DefaultListModel();
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);

        /*Initializing all the buttons and adding actionListener to the 
         * So as to recognise when the button is pressed */
        stats = new JButton("Statistics");
        StatsListener stat= new StatsListener(stats);
        stats.addActionListener(stat);
        
        replace = new JButton(replaceString);
        ReplaceListener rep = new ReplaceListener(replace);
        replace.setActionCommand(replaceString);
        replace.addActionListener(rep);
        
        
        addto = new JButton(addString);
        AddListener2 addListener = new AddListener2(addto);
        addto.setActionCommand(addString);
        addto.addActionListener(addListener);
        addto.setEnabled(false);

        addList = new JButton(addlist);
        AddListenerList addListener1 = new AddListenerList(addList);
        addList.setActionCommand(addlist);
        addList.addActionListener(addListener1);
        addList.setEnabled(true);

        help = new JButton(helpString);
        HelpListener helplistener = new HelpListener(help);
        help.setActionCommand(helpString);
        help.addActionListener(helplistener);
        help.setEnabled(true);

        dict = new JButton(dictString);
        DictListener dictlistener = new DictListener(dict);
        dict.addActionListener(dictlistener);

        file = new JButton(fileString);
        FileListener filelistener = new FileListener(file);
        file.addActionListener(filelistener);

        ignore = new JButton(ignoreString);
        IgnoreListener ignorelistener = new IgnoreListener(ignore);
        ignore.addActionListener(ignorelistener);

        compare = new JButton(compareString);
        CompareListener comparelistener = new CompareListener(compare);
        compare.addActionListener(comparelistener);

        Addwords = new JTextField(10);
        Addwords.addActionListener(addListener);
        Addwords.getDocument().addDocumentListener(addListener);


        /*Initializing the panels used in the GUI */
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,
                                           BoxLayout.LINE_AXIS));
        buttonPane.add(addList);
        buttonPane.add(help);
        buttonPane.add(addto);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(Addwords);
        buttonPane.add(dict);
        buttonPane.add(file);
        buttonPane.add(ignore);
        buttonPane.add(compare);
        buttonPane.add(replace);
        buttonPane.add(stats);
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        /*Adding the panels to the main frame*/
        add(listScrollPane, BorderLayout.CENTER);
        add(buttonPane, BorderLayout.PAGE_END);
    }
    /**Ignore Listener that activates when the ignore button is pressed.
     * It removes the selected word from the List*/
    class ReplaceListener implements ActionListener{
    	JButton button;
    	public ReplaceListener(JButton button){
    		this.button=button;
    	}
		
		public void actionPerformed(ActionEvent e) {
			 JFrame replaceframe = new JFrame("DictionaryGUIDemo");
		        replaceframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


		        JComponent replacePane = new Replace(textfileName);
		        replacePane.setOpaque(true);
		        replaceframe.setContentPane(replacePane);

		        //Display the window.
		        replaceframe.pack();
		        replaceframe.setVisible(true);
			}
		
		}
    	
    class StatsListener implements ActionListener{
    	JButton button;
    	public StatsListener(JButton button){
    		this.button = button;
    	}
    	public void actionPerformed(ActionEvent e){
    		JFrame statsframe = new JFrame("DictionaryGUIDemo");
	        statsframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


	        JComponent statsPane = new Replace(wordsinFile,wordstoDictionary,wordsIgnored,linesreadInput,textfileName);
	        statsPane.setOpaque(true);
	        statsframe.setContentPane(statsPane);

	        //Display the window.
	        statsframe.pack();
	        statsframe.setVisible(true);
    	}
    }
    class IgnoreListener implements ActionListener{

		JButton button;
		public IgnoreListener(JButton button){
			this.button = button;
		}
		public void actionPerformed(ActionEvent e) {
			int index=list.getSelectedIndex();
			wordsIgnored++; 
			ignoreArr.add((String) listModel.getElementAt(index));
			 listModel.remove(index);
		
		}

    }
    /**CompareListener that activates when the compare files button is pressed
     * Opens the text file and then compared it with the hashtable containing the
     * Dictionary words. All the different words are then added to the List*/
    class CompareListener implements ActionListener{

		JButton button;
		public CompareListener(JButton button){
			this.button = button;
		}
		public void actionPerformed(ActionEvent e) {
			listModel.clear();
		dictionary.create(directionary+DictfileName);
		String line="Text.txt";
		try {

	        FileReader textReader =
	            new FileReader(directionary+textfileName);
	        BufferedReader textbufferedReader =
	            new BufferedReader(textReader);


	        while((line = textbufferedReader.readLine()) != null) {
	        	linesreadInput++;
	            if(dictionary.contain(line)==false){
	            	//line=line.replace("[a-zA-Z]","");
	            	if(listModel.contains(line)==false && ignoreArr.contains(line)==false)
	            		wordsinFile++;
	            	listModel.addElement(line);
				}
	        }


	        textbufferedReader.close();
	       
	    }
	    catch(FileNotFoundException ex) {
	    	if (DictfileName.equals("empty")){
	    		JOptionPane.showMessageDialog(button,"Please enter a file name first");
	    	}
	    	else{
	        JOptionPane.showMessageDialog(button,"Unable to open file '" +
	            textfileName+ "'");}
	    }
	    catch(IOException ex) {
	        System.out.println(
	            "Error reading file '"
	            + textfileName + "'");

	      }
		}

    }

    /**HelpListener activates when help button is pressed
     * Displays a window with all the possible options*/
    class HelpListener implements ActionListener{
    	JButton button;
    	public HelpListener(JButton button){
    		this.button = button;
    	}
    	public void actionPerformed(ActionEvent e){
    		int reply = JOptionPane.showConfirmDialog(button, "Do you want help with the Application!");
            if(reply == JOptionPane.YES_OPTION)
            {
              JOptionPane.showMessageDialog(button,HelpText);
            }

    	}
    }
    /**DictListener activates when button is pressed.
     * Prompts user for DictionaryFile name
     * and checks whether the file exists or not */
    class DictListener implements ActionListener{
    	private JButton button;

    	public DictListener(JButton button){
    		this.button = button;
    	}

		public void actionPerformed(ActionEvent e) {
			
			 DictfileName =(
		            JOptionPane.showInputDialog("Type in Dictionary File Name",""));
			 File checkfile = new File(directionary+DictfileName);
				if(checkfile.exists()==false){
					JOptionPane.showMessageDialog(button,"File does not exist");
					return;
				}

    }
		}
    /**TextListener activates when text file button is pressed.
     * Prompts user for Input File name
     * and checks whether the file exists or not */
    class FileListener implements ActionListener{
    	private JButton button;

    	public FileListener(JButton button){
    		this.button = button;
    	}

		public void actionPerformed(ActionEvent e) {
			ignoreArr.clear();
			textfileName = (
		            JOptionPane.showInputDialog("Type in Text File Name",""));
				File checkfile = new File(directionary+textfileName);
				if(checkfile.exists()==false){
					JOptionPane.showMessageDialog(button,"File does not exist");
					return;
				}

    }
		}
    /**AddListenerList activates when button is pressed.
     *add the selected word to the List */
    class AddListenerList implements ActionListener {
    	JButton button;
    	private boolean alreadyEnabled = false;
        public AddListenerList(JButton addList) {
			this.button = addList;

        
		}

		public void actionPerformed(ActionEvent e) {
			if (list.isSelectionEmpty()){
        		return;
        	}

			int index = list.getSelectedIndex();
			if(DictfileName=="empty"){
				JOptionPane.showMessageDialog(button,"Dictionary File not chosen");
				return;
			}
			dictionary.addToDictionary((String) listModel.elementAt(index));
		
            File dictFile1 = new File(directionary+DictfileName);
            try {
            	FileWriter writeFile = new FileWriter(dictFile1,true);
            	BufferedWriter addFile = new BufferedWriter(writeFile);
            	addFile.write('\n'+(String) listModel.elementAt(index));
            	wordstoDictionary++;
            	addFile.close();
			}
			catch (IOException oops)
			{
				System.out.println(oops);
			}
            listModel.remove(index);

            int size = listModel.getSize();

            	if (size==0){
            		button.setEnabled(false);
            	}
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }

                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);

        }
    }

    /**AddListener2 activates when add to list button is pressed.
     * adds the word in the textfield into the list */
    class AddListener2 implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;

        public AddListener2(JButton button) {
            this.button = button;
        }


        public void actionPerformed(ActionEvent e) {
            String name = Addwords.getText();
            int size = listModel.getSize();
            
            if (name.equals("") || alreadyInList(name)) {
                
                Addwords.requestFocusInWindow();
                Addwords.selectAll();
                return;
            }

            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }

            listModel.insertElementAt(Addwords.getText(), index);

            //Reset the text field.
            Addwords.requestFocusInWindow();
            Addwords.setText("");

            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }


        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        }

        //Required by DocumentListener.
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        //Required by DocumentListener.
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        //Required by DocumentListener.
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }

    }


    //This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {

            if (list.getSelectedIndex() == -1) {

                help.setEnabled(false);

            } else {

                help.setEnabled(true);
            }
        }
    }

    /**Create method which creates the main frame and in conjunction
     * with the main method and creates a GUI*/

    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("DictionaryGUIDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JComponent newContentPane = new DictionaryDemo();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    /*Main method that runs the GUI as an applet*/
    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

}