/**
* @author [Nikhil Netraganti,Jason Pajas,Andrew Wingersky,Logan Jepson, Nicholas Creasman ]
* @version [1.1]
* @param[filename][create takes a string for filename to populate a dictionary object]
* @return[boolean value will be returned when checking if dictionary contains a word]
* @exception[IOException][IOException will be thrown if unable to populate a dictionary object given an incorrect file name] 
 */
 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
 

public class Dictionary {
	
	private int prime = 1427;
	private Slot[] array;

	/**Dictionary is the constructor for the dictionary object
	 * @param none
	 * @return none
	 */
	public Dictionary() { //constructor to initialize dictionary object
		this.prime = prime;
		array = new Slot[prime];
		for (int i = 0; i < prime; i++) { //create slots
			array[i] = new Slot();
		}
	}
	
	/**create creates the dictionary object from contents of some file
	 * @param string	name of file to parse
	 * @return void
	 */
	public void create(String fileName) { //populate the slots of the dictionary object with the contents of a textfile
		try {
			BufferedReader read = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = read.readLine()) != null) { //untile end of file
				addToDictionary(line.toLowerCase()); //convert the word to lower case for easy handling
			}
		}
		catch (IOException ioe) { //error handling
			ioe.printStackTrace();
		}
	}
	
	/**addToDictionary adds a line/word to the created dictionary object
	 * add a word to the dictionary boject
	 * @param string	the string represents the word that should be added to the dictionary object
	 */
	public void addToDictionary(String lineToAdd) { //add a line/word to the dictionary object
		array[hash(lineToAdd)].put(lineToAdd);
	}
	
	/**hash locates the correct slot into which a word should be inserted
	 * @param string	the string represents the word being added
	 * @return int		the int represents the proper slot location into which the string word should be added
	 */
	private int hash(String lineToAdd) { //locate the proper slot to insert a line into the dictionary object
		return (lineToAdd.hashCode() & 0x7fffffff) % prime;
	}
	
	/**contain checks the dictionary object to see if a word is already stored in the dictionary object
	 * @param string	the string represents the word being added
	 * @return boolean	return true if the line is stored in the dictionary, false if it is not stored
	 */
	public boolean contain(String lineToAdd) { //check if a line or word is stored in the dictionary object
		lineToAdd = lineToAdd.toLowerCase();
		return array[hash(lineToAdd)].get(lineToAdd);
	}
	
	class Slot {
		
		private Node head;
		
		/**get checks if the input string is currently within the current slot
		 * @param string	the string represents the input word
		 * @return boolean	the boolean value returns true if the input string is in the current slot, false otherwise
		 */
		public boolean get(String input) { //check if input string is within the current slot
			Node next = head;
			while (next != null) { //until end of node linked list
				if (next.word.equals(input)) {
					return true;
				}
				next = next.next;
			}
			return false;
		}
		
		/**put inserts a line/word into the slot
		 * @param string	the string represents the line being added to the slot
		 * @return void
		 */
		public void put (String lineToAdd) { //insert a line to the slot
			for (Node current = head; current != null; current = current.next) { //until end of node linked list
				if (lineToAdd.equals(current.word)) {
					return;
				}
			}
			head = new Node(lineToAdd, head);
		}
		
		class Node {
			
			String word;
			Node next;
			
			/**Node is the constructor for the node object
			 * @param string, Node	the string represents the line added, while the node is the next node, connecting the slots together
			 * @return none
			 */
			public Node(String lineToAdd, Node next) { //constructor for node objects, connects slots to each other
				this.word = lineToAdd;
				this.next = next;
			}
		}
	}
}