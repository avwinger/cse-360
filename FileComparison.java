/**
 *@author [Nikhil Netraganti,Jason Pajas,Andrew Wingersky,Logan Jepson, Nicholas Creasman ]
* @version [1.1]
* compares the files
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;

    public class FileComparison {
        public static void main(String[] args) throws Exception {

            BufferedReader testReader = null;
            BufferedReader dictionaryReader = null;

            String currentLine;
            List<String> testList = new ArrayList<String>();
            List<String> dictionaryList = new ArrayList<String>();

            testReader = new BufferedReader(new FileReader("test.txt"));
            dictionaryReader = new BufferedReader(new FileReader("dictionary.txt")); //istance variables for comparison

            while ((currentLine = testReader.readLine()) != null) //until end of test file
            {
                testList.add(currentLine);
            }

            while ((currentLine = dictionaryReader.readLine()) != null) //until end of dictionary file
            {
                dictionaryList.add(currentLine);
            }

            List<String> differenceList = new ArrayList<String>(testList); //create arraylist for differences
            differenceList.removeAll(dictionaryList); //remove differences from dictionary

            for(int i=0; i < differenceList.size(); i++) //until end of difference print different words
            {
                System.out.println(differenceList.get(i));
            }
        }
    }