/**
 *@author [Nikhil Netraganti,Jason Pajas,Andrew Wingersky,Logan Jepson, Nicholas Creasman ]
* @version [1.1]
* @exception[IOException][IOException will be thrown if unable to replace] 
 */
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;
public class Replace extends JPanel implements ListSelectionListener {
	private JList replaceList;
	private DefaultListModel<String> replaceModel;
	private JButton replace;
	private JButton cancel;
	private JButton saveStats;
	private JTextField replaceWords;
	private JTextArea statNums;
	public int wordsinFile=0;
	public static int wordsReplaced=0;
	public int wordstoDictionary=0;
	public int wordsIgnored=0;
	public int linesreadInput=0;
	public String textfileName="";
	public String dictionary;
    public String directionary="/Users/LoganJepson/Documents/";
    //instance variables
    
    /**Replace creates the GUI for the replace functionality
     *@param int, int, int, int, string, string		takes in an int representing how many words are in the file, how many words are in the dictionary,
     *how many words are ignored, how many lines are read from input, the string of the word, and the string for the dictionary file name
     *@return none
     */
	    
    public Replace(int wordsinFile2, int wordstoDictionary2, int wordsIgnored2, int linesreadInput2,String name,String dictfileName){
    	super(new BorderLayout());
    	dictionary=dictfileName;
    	textfileName=name;
    	wordsinFile=wordsinFile2;
    	wordstoDictionary=wordstoDictionary2;
    	wordsIgnored=wordsIgnored2;
    	linesreadInput=linesreadInput2;
    	
    	statNums = new JTextArea();
    	statNums.setTabSize(WIDTH);
    	statNums.setEditable(false);
    	String statPage = "Words in File: "+wordsinFile+
    			"\n\n"+"Words Replaced: "+wordsReplaced+
    			"\n\n"+"Words added to Dictionary: "+wordstoDictionary+
    			"\n\n"+"Words ignored: "+wordsIgnored+
    			"\n\n"+"Lines read from Input File: "+linesreadInput;
    	statNums.setText(statPage);
    	saveStats = new JButton("Save Statistics for this pair");
    	StatisticsListener stat = new StatisticsListener(saveStats);
    	saveStats.addActionListener(stat);
    	add(statNums);
    	add(saveStats,BorderLayout.PAGE_END);
    }
   /**Replace base replace GUI
    * @param string	string name represents the string of the word replaced
     * @return none
	*/
    public Replace(String name){
	    	
	    	super(new BorderLayout());
	    	textfileName = name;
	    	replaceModel = new DefaultListModel();
	        replaceList = new JList(replaceModel);
	        replaceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        replaceList.setSelectedIndex(0);
	        replaceList.addListSelectionListener(this);
	        replaceList.setVisibleRowCount(5);
	        JScrollPane replaceScroll = new JScrollPane(replaceList);
	        
	        replace = new JButton("Replace");
	        ReplaceListener rep = new ReplaceListener(replace);
	        replace.addActionListener(rep);
	        
	        replaceWords = new JTextField();
	        JPanel inputPanel = new JPanel();
	        inputPanel.setLayout(new BoxLayout(inputPanel,
                    BoxLayout.LINE_AXIS));
	        inputPanel.add(replace);
	       
	        inputPanel.add(replaceWords);
	        String line = "blah";
			try {

		        FileReader textReader =
		            new FileReader(directionary+textfileName);
		        BufferedReader textbufferedReader =
		            new BufferedReader(textReader);


		        while((line = textbufferedReader.readLine()) != null) {
		            	if(replaceModel.contains(line)==false)
		            	replaceModel.addElement(line);
		            	
					}
		        
		        textbufferedReader.close();}
		       
		    
		    catch(FileNotFoundException ex) {
		        System.out.println(
		            "Unable to open file '" +
		            textfileName+ "'");
		    }
		    catch(IOException ex) {
		        System.out.println(
		            "Error reading file '"
		            + textfileName + "'");

		      }
	        add(replaceScroll, BorderLayout.CENTER);
	        add(inputPanel,BorderLayout.PAGE_END);
	        
	        
	    }
	    
	  
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	class ReplaceListener implements ActionListener{
		
	JButton button;
	/**ReplaceListener manages the replace button
	 * @param button	button is the repalce button
	 * @return none
	 */
	public ReplaceListener(JButton button){
		this.button = button;
		}
	/**actionPerformed performes the replace action
	 * @param ActionEvent 	represents the action
	 * @return void
	 */
	
	public void actionPerformed(ActionEvent e) {
		if(replaceWords.getText().equals("")){
		return;
		}
		String word = replaceWords.getText();
		wordsReplaced++;
		String line;
		int index = replaceList.getSelectedIndex();
		
		try {
			
			removeLineMethod(replaceModel.getElementAt(index));
		} catch (IOException e1) {
		
			e1.printStackTrace();
		}
		replaceModel.setElementAt(word, index);
		String line1 = "blah";
     	JOptionPane.showMessageDialog(button,"Words Replaced");
	}
	}
class StatisticsListener implements ActionListener{
	JButton button;
	public StatisticsListener(JButton button){
		this.button=button;
	}
	public void actionPerformed(ActionEvent e) {
		try{
			File statFile = new File("Statfile for "+textfileName+" and "+dictionary);
			statFile.createNewFile();
			PrintWriter writer = new PrintWriter(new FileWriter(statFile));
			String statPage = "Words in File: "+wordsinFile+
	    			"\n\n"+"Words Replaced: "+wordsReplaced+
	    			"\n\n"+"Words added to DIctionary: "+wordstoDictionary+
	    			"\n\n"+"Words ignored: "+wordsIgnored+
	    			"\n\n"+"Lines read from Input File: "+linesreadInput;
			writer.write(statPage);
			JOptionPane.showMessageDialog(button,"The file has been created.");
			writer.close();
			
		}
		catch(IOException e1){
			JOptionPane.showMessageDialog(button,"Please enter a file name first");
		}
	}
}
	  
	/**removeLineMethod removes the line from the dictionary
	 *@param string		linetoRemove represents the line being removed
	 *@return void
	 */
	public void removeLineMethod(String lineToRemove) throws IOException {
		
		
		File inputFile = new File(textfileName);
		   File tempFile = new File("TempWordlist.txt");
		   tempFile.createNewFile();
		   int instance=0;
		   
		  
		   PrintWriter writer = new PrintWriter(new FileWriter(tempFile));
		   String currentLine = "\n";
		   String word = replaceWords.getText();
		   int index=0;
		  while(index!=replaceModel.size()){
		  
			  if(!replaceModel.getElementAt(index).equals(lineToRemove)){
			  writer.write(replaceModel.getElementAt(index) + "\n");
			  }
			  else{
			  instance++;
			  
			  }
			  index++;
		  }
		  for(int i=0;i<=instance;i++){
		  writer.write(word + currentLine);}
		   writer.close();
		   wordsReplaced++;
		   tempFile.renameTo(inputFile);
		 }
	}
